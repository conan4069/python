# -*- coding: utf-8 -*-
{
    'name': "ga_ir_module",

    'author': "ESCM Guardián del ALBA.",

    'depends': ['base'],

    # always loaded
    'data': [
        'views/ga_ir_module.xml',
    ],

    'installable': True,
	'auto_install': True,
}